package br.ifsc.edu.nichollas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth= FirebaseAuth.getInstance();

        //Create new account
        mAuth.createUserWithEmailAndPassword("nico@ifsc.edu.br","123mudar");
        mAuth.signInWithEmailAndPassword("nico@ifsc.edu.br","123mudar").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(getApplicationContext(),mAuth.getCurrentUser().getEmail(),Toast.LENGTH_LONG).show();
                    Intent i=new Intent(getApplicationContext(),PrincipalActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(),"Falha no login",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
